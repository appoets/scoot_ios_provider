//
//  PaymentWebVC.swift
//  Goboda
//
//  Created by Sethuram Vijayakumar on 14/05/21.
//  Copyright © 2021 Appoets. All rights reserved.
//

import UIKit
import WebKit

class PaymentWebVC: UIViewController {
    
    var webView: WKWebView!
    var payPalUrl: String!
    var payMentSuccessCompletion: (() -> ())?
    var payMentFailureCompletion: (() -> ())?
    var paymentDummySucess: (() -> ())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = false
        setNavigationBar()
        let leftButton = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_back").imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(tapBack))
        self.navigationController?.navigationBar.tintColor = .black
        self.navigationItem.leftBarButtonItem = leftButton
        self.title = "DPO Payments"
        webViewSetup()
        loadWebUrl(urlString: payPalUrl)
    }
    
    private func setNavigationBar() {
        self.setNavigationTitle()
        self.setLeftBarButtonWith(color: .black)
        self.title = TaxiConstant.paymentVia.localized
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    @objc private func tapBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func webViewSetup() {
        webView = WKWebView()
        webView.navigationDelegate = self
        webView.tag = 111
        view = webView
    }
    
    func loadWebUrl(urlString: String) {
        if let url = URL(string:urlString ) {
            webView.load(URLRequest(url: url))
        }
    }
}


extension PaymentWebVC : WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        if let url = (webView.url?.absoluteString) {
            
            
        }
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        if let url = (webView.url?.absoluteString) {
            print(url)
            //            _ = URL(string: url)!
       
            if url.contains("https://demo.scoot.co.bw/provider/wallet"){
                //             let status = newURL.valueOf("txnref")
                self.payMentSuccessCompletion?()
                self.navigationController?.popViewController(animated: true)
                
            }else if url.contains("https://demo.scoot.co.bw/dpo/failure"){
                self.payMentFailureCompletion?()
                self.navigationController?.popViewController(animated: true)
                
            }
        }
    }
}

extension URL {
    func valueOf(_ queryParamaterName: String) -> String? {
        guard let url = URLComponents(string: self.absoluteString) else { return nil }
        return url.queryItems?.first(where: { $0.name == queryParamaterName })?.value
        
    }
    
}
